#!/bin/bash

#set up cron job
crontab -l > cronjobs
echo "0 */10 * * * $(pwd)/download.sh">>cronjobs
echo "0 0 * * * $(pwd)/ngezip.sh">>cronjobs
crontab cronjobs

#bikin file baru yang berguna untuk bikin direktori baru dan download gambar
echo '#!/bin/bash

cd $(pwd)

#pengambilan variabel waktu untuk ngedownload gambar sesuai waktunya, berdasarkan soal
timestamp=$(date +"%H")
if [ $timestamp -eq 0 ]; then
  downloads=1
else
  downloads=$timestamp
fi

#berguna untuk cek udah berapa banyak folder "kumpulan_* di direktori tsb
num_folders=$(ls -d kumpulan_* | wc -l)

#berguna untuk memberi nama direktori dan mkdir berfungsi untuk bikin direktori berdasarkan dir_name
dir_name="kumpulan_$((num_folders+1))"
mkdir $dir_name

#looping untuk download foto sesuai dengan jumlah waktunya
for (( i=1; i<=$downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget -O "$dir_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done'>download.sh

#bikin file baru yang berguna untuk ngezip direktori
echo '#!/bin/bash
cd $(pwd)

#berguna untuk mendapatkan jumlah direktori dengan nama kumpulan_*
num_folders=$(ls -d kumpulan_* | wc -l)
#berguna untuk mendapatkan jumlah file dengan nama devil_*
num_zip=$(ls -d devil_* | wc -l)
#variabel nama zip
zip_name="devil_$(($num_zip+1))"
#looping untuk ngezip beberapa direktori dan menghapus direktori yang telah di zip
for (( i=1; i<=$num_folders; i++ ))
do 
  zip -r $zip_name kumpulan_$i
  rm -r kumpulan_$i
done'>ngezip.sh

