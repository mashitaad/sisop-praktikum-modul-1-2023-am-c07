#!/bin/bash

echo -e "\nNOMOR 1 A"
awk -F ',' '$4=="Japan" {print $2 "," $4}' '2023 QS World University Rankings.csv' | head -n 5

echo -e "\nNOMOR 1 B"
awk -F ',' '$4=="Japan" {print $2 "," $9}' '2023 QS World University Rankings.csv' | head -n 5 | sort -t, -k2 | tail -n 5 | awk -F ',' '{print $1}' 

echo -e "\nNOMOR 1 C"
awk -F ',' '$4=="Japan" {print $20 "," $2}' '2023 QS World University Rankings.csv' | sort -t, -k1 -n | head -n 10 | awk -F ',' '{print $1 "," $2}'

echo -e "\nNOMOR 1 D" 
awk -F ',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
