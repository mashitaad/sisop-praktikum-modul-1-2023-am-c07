#!/bin/bash

now_hour=$(date "+%H")
now_date=$(date "+%H:%M %d:%m:%Y")

to_lowercase=(abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz)
to_uppercase=(ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ)
cat /var/log/syslog | tr "${to_lowercase:0:26}${to_uppercase:0:26}" "${to_lowercase:${now_hour}:26}${to_uppercase:${now_hour}:26}" > "/home/mashitaad/Downloads/edi/encrypt/${now_date}.txt"
