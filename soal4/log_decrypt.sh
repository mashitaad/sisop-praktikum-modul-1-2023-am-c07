#!/bin/bash

now_hour=$(date "+%H")
encrypt_file=$(ls -t /home/mashitaad/Downloads/edi/encrypt | head -n1)
to_lowercase=(abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz)
to_uppercase=(ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ)

if [ -f "/home/mashitaad/Downloads/edi/encrypt/${encrypt_file}" ]; then
    cat "/home/mashitaad/Downloads/edi/encrypt/${encrypt_file}" | tr "${to_lowercase:${now_hour}:26}${to_uppercase:${now_hour}:26}" "${to_lowercase:0:26}${to_uppercase:0:26}" > "/home/mashitaad/Downloads/edi/decrypt/$(basename ${encrypt_file})"
fi
