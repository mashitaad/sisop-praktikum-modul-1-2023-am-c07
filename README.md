# sisop-latihan-modul-1-2023-AM-C07

## Identitas Kelompok
| Name            | NRP        |
| ---             | ---        |
| Mashita Dewi    | 5025211036 |
| Syukra Wahyu R  | 5025211037 |
| Akhmad Mustofa S| 5025211230 | 

## Soal 1
1. Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
    - Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking         tertinggi di Jepang.
    - Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
    - Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
    - Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
## Penyelesaian
Langkah pertama adalah buka file QS World University Rankings di terminal anda 
```ruby
cat '2023 QS World University Rankings.csv'
```
Langkah kedua adalah buat file bernama university_survey.sh dengan teks editor nano
```ruby
nano university_survey.sh
```
Langkah ketiga adalah lakukan perintah soal nomor 1 poin a, yaitu menampilkan 5 universitas ranking tertinggi di Jepang
```ruby
awk -F ',' '$4=="Japan" {print $2 "," $4}' '2023 QS World University Rankings.csv' | head -n 5
```
Penjelasan :
- `awk` untuk melakukan filter
- `-F ','` untuk separator dalam bentuk ','
- `$4=="Japan"` untuk memberi pernyataan bahwa filter dilakukan pada kolom keempat yang memiliki kata kunci 'Japan'
- `{print $2 "," $4}` untuk menampilkan kolom kedua yaitu nama universitas dan negaranya yang sesuai filter yang diberikan sebelumnya
- `'2023 QS World University Rankings.csv'` menandakan bahwa filter dilakukan pada file csv tersebut
- `|` sebagai separator perintah
- `head -n 5` untuk mendapatkan hasil urutan 5 teratas
 
Langkah keempat adalah lakukan perintah soal nomor 1 poin b, yaitu menampilkan universitas dengan FSR Score terendah dari data hasil filter nomor 1 poin a
```ruby
awk -F ',' '$4=="Japan" {print $2 "," $9}' '2023 QS World University Rankings.csv' | head -n 5 | sort -t, -k2 | tail -n 5 | awk -F ',' '{print $1}' 
```
Penjelasan :
- `awk -F ',' '$4=="Japan" {print $2 "," $9}' '2023 QS World University Rankings.csv' | head -n 5` melakukan perintah untuk menampilkan 5 universitas dengan FSR Score tertinggi di Jepang
- `{print $2 "," $9}'` $9 menandakan mengambil data pada kolom ke-9 yaitu FSR Score
- `sort -t,` untuk mengurutkan hasil perintah sebelumnya secara ascending
- `-k2` melakukannya pada kolom kedua yaitu FSR Score
- `tail -n 5` mengambil 5 data universitas dengan FSR terendah hingga tertinggi
- `awk -F ',' '{print $1}'` menampilkan hasil dari perintah sebelumnya yaitu universitas dengan FSR Score terendah

Langkah kelima adalah lakukan perintah soal nomor 1 poin c, yaitu menampilkan 10 Universitas dengan ger tertinggi di Jepang
```ruby
awk -F ',' '$4=="Japan" {print $20 "," $2}' '2023 QS World University Rankings.csv' | sort -t, -k1 -n | head -n 10 | awk -F ',' '{print $1 "," $2}'
```
Penjelasan :
- `awk -F ',' '$4=="Japan" {print $20 "," $2}' '2023 QS World University Rankings.csv'` melakukan perintah untuk menampilkan 5 universitas dengan GER tertinggi di Jepang
- `{print $20 "," $2}'` $20 menandakan mengambil data pada kolom ke-20 yaitu GER
- `sort -t,` untuk mengurutkan hasil perintah sebelumnya secara ascending
- `-k1 -n` melakukan sort pada kolom 1 yaitu kolom GER
- `|` sebagai separator perintah
- `head -n 10` untuk mendapatkan hasil urutan 10 teratas
- `awk -F ',' '{print $1 "," $2}'` menampilkan hasil dari perintah sebelumnya yaitu universitas dengan GER tertinggi

Langkah keenam adalah lakukan perintah soal nomor 1 poin d, yaitu menampilkan universitas yang memiliki kata kunci "Keren"
```ruby
awk -F ',' '/Keren/ {print $2}' '2023 QS World University Rankings.csv'
```
Penjelasan :
- `awk -F ','` untuk mengubah field separator menjadi ','
- `'/Keren/` menemukan kata kunci 'Keren'
- `{print $2} '2023 QS World University Rankings.csv'` menampilkan kolom kedua pada file csv yaitu kolom nama universitas


## Soal 2
2. Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
    - Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X         kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap         10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
        - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
        - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat             (kumpulan_1, kumpulan_2, dst) 
    - Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip       “devil_NOMOR ZIP” dengan NOMOR.ZIPadalah urutan folder saat dibuat (devil_1, devil_2, dst). 

## Penyelesaian
Soal ini terdapat 2 proses penting yaitu download dan nge zip folder. Untuk itu dibuat script yang ketika di eksekusi dapat membuat 2 file tersebut.
 
1. Penjelasan file download
```ruby 
timestamp=$(date +"%H")
```

- `$(date +"%H")`berguna untuk mendapatkan waktu pada saat script di eksekusi
 ```ruby
num_folders=$(ls -d kumpulan_* | wc -l)
```
- `ls -d kumpulan_*|wc -l` berguna untuk mendapat jumlah file/direktori dengan nama kumpulan_

```ruby
dir_name="kumpulan_$((num_folders+1))"
mkdir $dir_name
```

- `dir_name`menyimpan nama direktori yang akan disimpan

- `mkdir $dir_name` membuat direktori baru dengan nama dari dir_name
```ruby
for (( i=1; i<=$downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget -O "$dir_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
```
- `for (( i=1; i<=$downloads; i++ ))`merupakan perulangan/looping yang akan dijalankan dengan counter download, download itu sendiri bernilai sesuai dengan jam pada saat itu.

- `filename="perjalanan_$i.jpg"` merupakan variabel untuk jadi nama file setelah di download.


- `wget -O "$dir_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"`
berguna untuk mengunduh foto dari web yang tertera.


2. Penjelasan file ngezip
```ruby
num_zip=$(ls -d devil_* | wc -l)
```

- `ls -d devil_*|wc -l` berfungsi untuk mendapat jumlah file dengan nama devil_

- `zip_name="devil_$(($num_zip+1))"`
merupakan variabel yang menyimpan nama file setelah di zip

```ruby
for (( i=1; i<=$num_folders; i++ ))
do 
  zip -r $zip_name kumpulan_$i
  rm -r kumpulan_$i
done'
```

- `zip -r $zip_name kumpulan_$i`berguna untuk ngezip semua folder dengan nama kumpulan_* kedalam 1 zip dengan nama zip_name
- `rm -r kumpulan_$i` berguna untuk menghapus semua direktori yang telah berhasil disimpan ke dalam zip

3. Penjelasan cronjob
- `crontab -l > cronjobs` mengambil template cronjob dan di write ke dalam file dengan nama cronjobs.
```ruby 
echo "0 */10 * * * bash /home/syukra/prak/download.sh">>cronjobs
```
- `echo " ">>cronjobs` perintah menambahkan " " di akhri file cronjobs
- `0 */10 * * * bash /home/syukra/prak/download.sh`cronjob yang akan mengeksekusi file setiap 10 jam
```ruby
echo "1 0 * * * bash /home/syukra/prak/ngezip.sh">>cronjobs
```
- `1 0 * * * bash /home/syukra/prak/ngezip.sh`cronjob yang akan mengeksekusi file setiap pukul 00:01
```ruby
crontab cronjobs
```
berguna untuk menambahkan cronjobs ke dalam crontab.

## Soal 3
3. Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
    - Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
        - Minimal 8 karakter
        - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
        - Alphanumeric
        - Tidak boleh sama dengan username 
        - Tidak boleh menggunakan kata chicken atau ernie
    - Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang         dilakukan user.
        - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
        - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
        - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
        - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
## Penyelesaian

### SISTEM REGISTER
Langkah pertama buat agar program dapat menerima username dengan cara berikut
```ruby
read -p "Enter your username: " username
```

Langkah kedua adalah membuat program agar sesuai dengan kondisi
`if grep -q ${username} ./users.txt` grep digunakan untuk menangkap string yang nantinya di compare ke dalam file users.txt untuk melakukan pengecekan username yang sama
```ruby
then 
	echo "Username already exist, please enter new username!"
	echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt
	read -p "Enter your username: " username
fi
```
Lalu atas perintah grep, jika terdapat username yang sama maka program akan menampilkan kalimat diatas dan memberi perintah user untuk mengetik usernamenya kembali 
- `$(date +"%y/%m/%d %H:%M:%S")` digunakan untuk mencatat timestamp yang dimasukkan ke dalam file log.txt
- `>>./log.txt` memasukkan ke dalam file log.txt
- `fi` sebagai break

Langkah ketiga adalah menerima input password dari user
```ruby
read -p "Enter your password: " password
```

Langkah keempat adalah melakukan perintah agar password sesuai dengan kondisi yang telah ditentukan
``` ruby
if [[ ! ${#password} -ge 8 ]]; then
	echo "Your password must contain 8 character or more"
elif [[ ! "$password" =~ [a-zA-Z0-9] ]]; then
	echo "Your password must be alphanumeric"
elif [[ ! "$password" =~ [A-Z] ]]; then
	echo "Your password must contain at least one uppercase letter"
elif [[ ! "$password" =~ [a-z] ]]; then
	echo "Your password must contain at least one lowercase letter"
elif [[ "$password" == *chicken* ]]; then
	echo "Your password must not contain the word 'chicken'"	
elif [[ "$password" == *ernie* ]]; then
	echo "Your password must not contain the word 'ernie'"
elif [[ "$password" == "$username" ]]; then
	echo "Your password must not be the same as username"
else 
	echo "Register Succesfully"
	echo "$(date +)"%y/%m/%d %H:%M:%S" REGISTER: INFO User $username registered succesfully">> ./log.txt
	echo $username $password>>./users.txt
fi
```
- `! ${#password} -ge 8 ` -ge adalah greater than, jadi dilakukan pengecekan apakah password user tidak lebih besar dari 8 atau dapat menggunakan perintah ini juga `${#password} -lt 8` yaitu lt adalah less than
- `! "$password" =~ [a-zA-Z0-9]` kondisi pengecekan apakah password tersebut tidak memenuhi alphanumeric (a-zA-Z0-9)
- `! "$password" =~ [A-Z]` kondisi yang mengecek apakah password tidak memenuhi karakter Uppercase
- `! "$password" =~ [a-z]` kondisi yang mengecek apakah password tidak memenuhi karakter lowercase
- `$password" == *chicken*` kondisi yang mengecek apakah password memiliki kata chicken
- `$password" == *ernie*` kondisi yang mengecek apakah password memiliki kata ernie
- `"$password" == "$username"` kondisi yang mengecek apakah password == username
```ruby
else 
	echo "Register Succesfully"
	echo "$(date +)"%y/%m/%d %H:%M:%S" REGISTER: INFO User $username registered succesfully">> ./log.txt
	echo $username $password>>./users.txt
fi
```
Jika kondisi - kondisi diatas telah terpenuhi maka, Register Succes dan username password akan dimasukkan ke file users.txt serta timestampnya dimasukkan ke log.txt.

### SISTEM LOGIN
Langkah pertama adalah menerima input dari user berupa string untuk username
```ruby
echo "Enter your username: "
read username
```

Langkah kedua adalah memeriksa apakah username yang telah diberikan terdapat dalam file users.txt
```ruby
if ! grep -q "^${username}" ./users.txt; then
        echo "USER NOT FOUND"
```
jika terdapat dalam file txt maka tampilkan "USER NOT FOUND"

Langkah ketiga adalah menyesuaikan password dengan kondisi yang telah ditentukan
```ruby
else
        password=$(grep "^${username} " ./user.txt | cut -d' ' -f2)
        echo "Enter your password"
        read passwordnow

        if [[ "$password" != "$passwordnow" ]]; then
                echo "Incorrect Password"
                echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed LOGIN ATTEMPT ON USER $username" >> ./log.txt
        else
                echo "Login succesfully"
                echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> ./log.txt
        fi
fi
```
Penjelasan :
- Jika username yang dimasukkan tidak terdapat pada user.txt `password=$(grep "^${username} " ./user.txt` maka lakuakan perintah berikutnya yaitu menerima input password `echo "Enter your password"`
- `cut -d' ' -f2` digunakan untuk pembaca sebuah string setelah karakter ' ' (spasi) yang terletak pada kolom kedua
- Lalu, jika password yang dimasukkan tidak sesuai dengan password yang dibuat sebelumnya `if [[ "$password" != "$passwordnow" ]]` akan ditampilkan error message `echo "Incorrect Password"`
- Tetapi, jika semua telah sesuai maka akan ditampilkan bahwa login sukses `echo "Login succesfully"` serta timestamp akan amsuk ke file log.txt `echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> ./log.txt`.

## Soal 4
4. Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. Log system tersebut harus  memiliki ketentuan : 
    - Backup file log system dengan format jam:menit tanggal:bulan:tahun.
    - Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
        - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b                diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
        - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
        - Setelah huruf z akan kembali ke huruf a
    - Backup file syslog setiap 2 jam untuk dikumpulkan 

## Penyelesaian

### Sistem Encrypt

- Langkah pertama untuk membuat sistem encrypt adalah membuat direktory decrypt untuk menyimpan file - file generate hasil encrypt melalui perintah `mkdir encrypt`
- Langkah pertama buat sistem encrypt dengan `nano log_encrypt.sh`
- Lalu buat kode seperti berikut :

```
now_hour=$(date "+%H")
```
- `now_hour`merupakan variabel yang menyimpan waktu pada saat file di eksekusi
- `$(date "+%H")`berguna untuk mendapatkan jam pada saat itu
```
now_date=$(date "+%H:%M %d:%m:%Y")
```
- `now_date` merupakan variabel yang menyimpan waktu lengkap pada saat file di eksekusi, variabel pada akhirnya akan digunakan untuk menjadi nama file.

- `$(date "+%H:%M %d:%m:%Y")`berguna untuk mendapatkan waktu lengkap pada saat file di eksekusi
```
to_lowercase=(abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz)
```

- `to_lowercase` variabel yang menyimpan karakter dalam huruf kecil
```
to_uppercase=(ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ)
```
 
- `to_uppercase` variabel yang menyimpan karakter dalam huruf besar
```
cat /var/log/syslog | tr "${to_lowercase:0:26}${to_uppercase:0:26}" "${to_lowercase:${now_hour}:26}${to_uppercase:${now_hour}:26}" > "/home/mashitaad/Downloads/edi/encrypt/${now_date}.txt"
```
- `cat /var/log/syslog` berfungsi untuk menampilkan syslog
- `tr` perintah untuk mengganti kata

### Sistem Decrypt

- Langkah pertama untuk membuat sistem decrypt adalah membuat direktory decrypt untuk menyimpan file - file generate hasil encrypt melalui perintah `mkdir decrypt`
- Langkah berikutnya untuk membuat sistem decrypt adalah buat file dengan `nano log_decrypt.sh`
- Lalu ketik kode seperti dibawah :

```
now_hour=$(date "+%H")
```
- `now_hour`merupakan variabel yang menyimpan jam pada saat itu
- `$(date "+%H")`berfungsi untuk mendapatkan jam pada saat itu
```
to_lowercase=(abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz)
```
- `to_lowercase` menyimpan karakter dalam lowercase, ditulis dua kali karena generatenya dua kali, terdapat cara yang lebih efektif yaitu dimodulo dengan 26, hal ini berlaku untuk variabel penyimpan uppercase juga
```
to_uppercase=(ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ)
```
- `to_uppercase` menyimpan karakter dalam uppercase

```
encrypt_file=$(ls -t /home/mashitaad/Downloads/edi/encrypt | head -n1)
```
- `encrypt_file`merupkan variabel yang menyimpan nama file hasil encript.
- `ls -t` berfungsi untuk menampilkan file dengan urutan waktu
- `head -n1` filter untuk mengambil file terbaru hasil dari sistem encrypt 

```ruby
if [ -f "/home/mashitaad/Downloads/edi/encrypt/${encrypt_file}" ]; then
    cat "/home/mashitaad/Downloads/edi/encrypt/${encrypt_file}" | tr "${to_lowercase:${now_hour}:26}${to_uppercase:${now_hour}:26}" "${to_lowercase:0:26}${to_uppercase:0:26}" > "/home/mashitaad/Downloads/edi/decrypt/$(basename ${encrypt_file})"
fi
```

- `-f`perintah memaksa untuk melakukan tindakan tertentu tanpa memeriksa atau meminta konfirmasi dari pengguna
- Jadi, dilakukan pengecekan dalam file "/home/mashitaad/Downloads/edi/encrypt/${encrypt_file", jika terdeteksi adanya file baru maka, sistem akan menampilkan file baru hasil encrypt tersebut yang perintahnya adalah `cat /home/mashitaad/Downloads/edi/encrypt/${encrypt_file}`
- Setelah mendapat file terbaru dari hasil encrypt maka sistem akan melakukan translate pengubahan melalui perintah `tr` 
- Translate dari file hasil encrypt (bahasa yang tidak kita pahami) melalui perintah berikut `"${to_lowercase:${now_hour}:26}${to_uppercase:${now_hour}:26}"` ke sebuah bahasa yang dapat kita pahami melalui perintah berikut `${to_lowercase:0:26}${to_uppercase:0:26}` 
- Yang nantinya hasil decrypt tersebut akan dimasukkan ke direktori decrypt yang sebelumnya sudah dibuat dengan perintah berikut `/home/mashitaad/Downloads/edi/decrypt/$(basename ${encrypt_file})`

### cronjob
- Langkah terakhir adalah melakukan generate otomatis sesuai permintaan soal yaitu "Backup file syslog setiap 2 jam untuk dikumpulkan" melalui cronjob
- Berikut adalah cara membuat cronjob :
    - Ketik `crontab -e` pada terminalmu, lalu terminal akan menampilkan GNU nano kosong dan isikan    perintah berikut: 

``` ruby
0 */2 * * * /bin/bash /home/mashitaad/Downloads/edi/log_encrypt.sh
0 */2 * * * /bin/bash /home/mashitaad/Downloads/edi/log_decrypt.sh
```

