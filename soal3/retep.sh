#!/bin/bash

echo "Enter your username: "
read username

if ! grep -q "^${username}" ./users.txt; then
        echo "USER NOT FOUND"
else
        password=$(grep "^${username} " ./user.txt | cut -d' ' -f2)
        echo "Enter your password"
        read passwordnow

        if [[ "$password" != "$passwordnow" ]]; then
                echo "Incorrect Password"
                echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed LOGIN ATTEMPT ON USER $username" >> ./log.txt
        else
                echo "Login succesfully"
                echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> ./log.txt
        fi
fi
