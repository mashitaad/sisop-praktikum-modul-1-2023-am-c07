#!/bin/bash

read -p "Enter your username: " username

if grep -q ${username} ./users.txt
then 
	echo "Username already exist, please enter new username!"
	echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists">>./log.txt
	read -p "Enter your username: " username
fi

read -p "Enter your password: " password

if [[ ! ${#password} -ge 8 ]]; then
	echo "Your password must contain 8 character or more"
elif [[ ! "$password" =~ [a-zA-Z0-9] ]]; then
	echo "Your password must be alphanumeric"
elif [[ ! "$password" =~ [A-Z] ]]; then
	echo "Your password must contain at least one uppercase letter"
elif [[ ! "$password" =~ [a-z] ]]; then
	echo "Your password must contain at least one lowercase letter"
elif [[ "$password" == *chicken* ]]; then
	echo "Your password must not contain the word 'chicken'"	
elif [[ "$password" == *ernie* ]]; then
	echo "Your password must not contain the word 'ernie'"
elif [[ "$password" == "$username" ]]; then
	echo "Your password must not be the same as username"
else 
	echo "Register Succesfully"
	echo "$(date +)"%y/%m/%d %H:%M:%S" REGISTER: INFO User $username registered succesfully">> ./log.txt
	echo $username $password>>./users.txt
fi


